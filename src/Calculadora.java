import java.util.Scanner;
/*****
 * Clase que me referencia el punto 1 del ejercicio
 */
public class Calculadora {
    public static void main(String[] args){
        try (Scanner entrada = new Scanner(System.in)){
            System.out.println("Por favor ingrese un número: ");
            double numero_1 = entrada.nextDouble();

            System.out.println("Por favor ingrese otro número");
            double numero_2 = entrada.nextDouble();

            //Operaciones
            double suma = numero_1 + numero_2;
            double resta = numero_1 - numero_2;
            double mult = numero_1 * numero_2;
            double div = numero_1 / numero_2;

            //Mostrar resultados
            System.out.println("La suma es: "+suma);
            System.out.println("La resta es: "+resta);
            System.out.println("La multiplicación es: "+mult);
            System.out.println("La divison es: "+div);
        } catch (Exception e) {
            //TODO: handle exception
        }
    }
}
