import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        /*
        String cadena = "5";
        //Convertir un string a un numero
        int numero = Integer.parseInt(cadena);
        numero = numero + 10;
        numero += 10;
        System.out.println("La suma es: "+numero);
        */

        //System.out.print("Por favor ingrese un string: ");
        //Entrada de datos(crear objeto Scanner)
        //Scanner entrada = new Scanner(System.in);
        //----MANIPULACIÓN DEL OBJETO----

        //Queda a la escucha de lo que ingrese el usuario
        /*
        String mensaje = entrada.next();
        System.out.println("Usted digitó: "+mensaje);
        */
        /*
        try {
            //Capturar un número
            System.out.println("Por favor ingrese número 1: ");        
            int numero_1 = entrada.nextInt();
            
            System.out.println("Por favor ingrese número 2: ");
            int numero_2 = entrada.nextInt();
            //Sumar
            int suma = numero_1 + numero_2;
            System.out.println("La sumatoria es: "+suma);
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("Error");
        }
        //Cerrar 
        entrada.close();
        */
        //Declarar variables
        Double numero_1 = 0.0;
        Double numero_2 = 0.0;
        int suma = 0;
        try (Scanner entrada = new Scanner(System.in)){
            //Capturar un número
            System.out.println("Por favor ingrese número 1: ");        
            numero_1 = entrada.nextDouble();
            
            System.out.println("Por favor ingrese número 2: ");
            numero_2 = entrada.nextDouble();

            double resultado = numero_1 / numero_2;
            System.out.println(resultado);

            //Capturar un decimal
            //entrada.nextDouble();
            //entrada.nextFloat();
            
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("Error al capturar los datos");
        }
        //Sumar
        //suma = numero_1 + numero_2;
        //System.out.println("La sumatoria es: "+suma);




    }
}
