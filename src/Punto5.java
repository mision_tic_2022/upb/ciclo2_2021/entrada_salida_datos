import java.util.Scanner;
/***
 * Autor: Darío Andrés Peña Quintero
 */
public class Punto5 {
    public static void main(String[] args){
        try (Scanner entrada = new Scanner(System.in)){
            System.out.println("Por favor ingrese un número: ");
            double numero_1 = entrada.nextDouble();

            System.out.println("Por favor ingrese otro número");
            double numero_2 = entrada.nextDouble();

            //Encontrar número mayor
            if(numero_1 > numero_2){
                System.out.println("El número mayor es: "+numero_1);
            }else if(numero_2 > numero_1){
                System.out.println("El número mayor es: "+numero_2);
            }else{
                System.out.println("Los números son iguales");
            }
            
            
        } catch (Exception e) {
            System.out.println("Error");
            //TODO: handle exception
        }
    }
}
