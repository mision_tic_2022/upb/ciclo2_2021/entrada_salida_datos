import java.util.Scanner;
/**
 * Autor: Juan Jose Arevalo
 */
public class Pares_impares {
    
    public static void main(String[] args) {
        int numero = 0;

        try (Scanner entrada = new Scanner(System.in)) {
            
            System.out.println("Ingrese el número: ");
            numero = entrada.nextInt();

            if (numero % 2 == 0) {
                
                System.out.println("El número es par"); 

            } else {
                System.out.println("El número es impar"); 
                
            }

        } catch (Exception e) {
            //TODO: handle exception
            System.out.print("Error");
        }

    }

}